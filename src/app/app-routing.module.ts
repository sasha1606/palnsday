import { NgModule } from '@angular/core';
import { Routes, RouterModule} from '@angular/router';

const routers: Routes = [
    {
        path: '',
        loadChildren: './site/site.module#SiteModule',
    },
    {
        path: '**',
        pathMatch: 'full',
        redirectTo: '/',
    }
];

@NgModule ({
    imports: [
        RouterModule.forRoot(routers),
    ],
    exports: [
        RouterModule,
    ],
})

export class AppRoutingModule {

}
