import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component ({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
})

export class LoginComponent implements OnInit {

    formLogin: FormGroup;

    constructor (
        private fb: FormBuilder,
    ) {}

    ngOnInit () {
        this.buildForm();
    }

    buildForm () {
        this.formLogin = this.fb.group({
            password: [
                '',
                Validators.required,
                Validators.minLength(4),
            ],
            email: [
                '',
                Validators.required,
                Validators.minLength(4),
            ],
        });
    }

    login () {
        console.log(1);
    }

}
