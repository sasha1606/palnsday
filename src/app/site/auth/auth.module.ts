import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';

import { AuthRoutingModule } from './auth-routing.module';
import { AuthService } from './auth.service';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';



@NgModule ({
    imports: [
        ReactiveFormsModule,
        CommonModule,
        AuthRoutingModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        HttpClientModule,
        MatCardModule,
    ],
    exports: [

    ],
    declarations: [
        LoginComponent,
        RegistrationComponent
    ],
    providers: [
        AuthService
    ]
})

export class AuthModule {

}
