import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { User } from './user';

@Injectable()

export class AuthService {

    // private header = new HttpHeaders();
    // header.append('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    // header.append('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // header.append('Access-Control-Allow-Origin', '*');

    httpOptions = {
        headers: new HttpHeaders({
            'Access-Control-Allow-Headers':  'X-Requested-With,content-type',
            'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
            'Access-Control-Allow-Origin': '*',
        })
    };

    constructor (
        private http: HttpClient,
    ) {}


    login () {

    }

    registration (data: User) {
        return this.http.post<User>(`${environment.path}/api/createUser`, data);
    }
}
