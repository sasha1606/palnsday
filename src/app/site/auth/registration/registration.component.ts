import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { User } from '../user';

@Component ({
    selector: 'app-registration',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.css'],
})

export class RegistrationComponent implements OnInit, OnDestroy {

    formRegistration: FormGroup;
    registration$: any = null;

    constructor (
        private fb: FormBuilder,
        private authService: AuthService,
    ) {}

    ngOnInit () {
        this.buildForm();
    }

    buildForm () {
        this.formRegistration = this.fb.group({
            name: [
                '',
                [
                    Validators.required,
                    Validators.minLength(4),
                ]
            ],
            nick_name: [
                '',
                [
                    Validators.required,
                    Validators.minLength(4),
                ]
            ],
            password: [
                '',
                [
                    Validators.required,
                    Validators.minLength(4),
                ]
            ],
            email: [
                '',
                [
                    Validators.required,
                    Validators.minLength(4),
                ]
            ],
        });
    }

    registration () {
        this.registration$ = this.authService.registration(this.formRegistration.value).subscribe(
         res => {
            console.log(res);
        },
        error => {
            console.error(error);
        },
        () => {
           console.log('complate');
        });
    }

    ngOnDestroy () {
        if (this.registration$) {
            this.registration$.unsubscribe();
        }
    }
}
