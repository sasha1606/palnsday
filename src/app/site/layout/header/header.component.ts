import { Component } from '@angular/core';

@Component ({
    selector: 'app-site-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})

export class HeaderComponent {
    public menu: MenuInterface[] = [
        {
            link: '/',
            name: 'main'
        },
        {
            link: '/contact',
            name: 'contact'
        },
        {
            link: '/about',
            name: 'about'
        },
    ];
}

class MenuInterface {
    link: string;
    name: string;
}
