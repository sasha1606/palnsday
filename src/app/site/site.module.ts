import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SiteComponent } from './site.component';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';

import { SiteRoutingModule } from './site-routing.module';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
    imports: [
        CommonModule,
        SiteRoutingModule,
        MatToolbarModule,
        MatButtonModule,
    ],
    declarations: [
        SiteComponent,
        HeaderComponent,
        FooterComponent
    ],
    exports: [

    ],
})

export class SiteModule {

}
