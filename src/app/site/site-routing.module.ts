import { NgModule } from '@angular/core';
import { Routes, RouterModule} from '@angular/router';

import { SiteComponent } from './site.component';

const routes: Routes = [
    {
        path: '',
        component: SiteComponent,
        children: [
            {
                path: '',
                loadChildren: './main/main.module#MainModule',
            },
            {
                path: 'contact',
                loadChildren: './contact/contact.module#ContactModule',
            },
            {
                path: 'about',
                loadChildren: './about/about.module#AboutModule',
            },
            {
                path: 'auth',
                loadChildren: './auth/auth.module#AuthModule',
            },
        ],

    },
];

@NgModule ({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [
        RouterModule,
    ],
})

export class SiteRoutingModule {

}
