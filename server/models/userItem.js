var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var UserItem = new Schema({
    name: {
        type: String,
        default: '',
        trim: true,
        required: true
    },
    nick_name: {
        type: String,
        default: '',
        trim: true,
        required: true
    },
    password: {
        type: String,
        default: '',
        trim: true,
        required: true
    },
    email: {
        type: String,
        default: '',
        trim: true,
        required: true
    },
});

module.exports = mongoose.model('users', UserItem);


