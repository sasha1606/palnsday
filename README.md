# Installation

1) git clone https://sasha1606@bitbucket.org/sasha1606/palnsday.git

2) npm install

3) ng server


# Start server

1) npm install -g nodemon OR npm install pm2 -g

2) nodemon server.js OR pm2 start server.js --watch


# Stop server if use pm2

1) pm2 stop server.js --watch


# Code Style and fix

1) ng lint

2) ng lint --fix


# Build to prod

1) ng build --prod


# TODO

1) Guard - canActivate 

2) interceptor localStorage headers

3) One test page admin

4) login and registration - css

5) Admin + resolve



