var express         = require('express'),
    path            = require('path'),
    mongoose        = require('mongoose'),
    logger          = require('morgan'),
    bodyParser      = require('body-parser'),
    appConfig       = require("./server/config"),
    router          = require("./server/routes/router"),
    app             = express();

//middlewares
app.use(express.static(__dirname));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json({type: 'application/json'}));
app.use(logger('dev'));
app.use('/', router);


// Add headers
// app.use(function (req, res, next) {
//
//     // Website you wish to allow to connect
//     res.setHeader('Access-Control-Allow-Origin', '*');
//
//     // Request methods you wish to allow
//     res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
//
//     // Request headers you wish to allow
//     res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
//
//     // Set to true if you need the website to include cookies in the requests sent
//     // to the API (e.g. in case you use sessions)
//     res.setHeader('Access-Control-Allow-Credentials', true);
//
//     // Pass to next layer of middleware
//     next();
// });

// connect to db
//The `useMongoClient` option is no longer necessary in mongoose 5.x, please remove it.
// mongoose.connect(appConfig.database, {
//     useMongoClient: true
// });
mongoose.connect(appConfig.database);

mongoose.connection.on('error', function(err) {
    console.log('Error: Could not connect to MongoDB');
});
mongoose.connection.once('open', function() {
    console.log('Connection to MongoDB is started');
});

// start server
app.listen(appConfig.port, function(){
    console.log('Express server listening on port 3000');
});